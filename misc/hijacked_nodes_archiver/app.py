import logging
from os import environ
from sys import stdout
from time import sleep, time

import jwt
from redis import Redis
from requests import RequestException, get
import yaml

OUTPUT_PROCESSING_HOST = environ.get("PROCESSING_NAME")
PORT = 3000
OUTPUT_PROCESSING_URL = "http://" + OUTPUT_PROCESSING_HOST + ":3000/get_stats"
JWT_SECRET = environ.get("JWT_SECRET")
NEW_CONFIG_FREQ = int(environ.get("DELAY", default="3600"))  # 1hr in seconds
ARCHIVE_ITER_SEC = NEW_CONFIG_FREQ // 2
REDIS_HOSTNAME = environ.get("REDIS_HOSTNAME")
logging.basicConfig(stream=stdout, level=logging.INFO)
logger = logging.getLogger()
redis_conn = Redis(host=REDIS_HOSTNAME)


def main():
    props = {
        "user": {
            "name": "archival container",
            "email": "archival@distrhijack",
            "role": "admin",
            "lastLogin": "",
        }
    }
    while True:
        overlay_names = list(map(lambda x: x.decode(), redis_conn.smembers("overlays")))
        if len(overlay_names) == 0:
            logger.info("sleeping for 5minutes to wait for stats to exist")
            sleep(300)  # wait for config generation to run for the first time
            continue

        props["iat"] = int(time())
        token = jwt.encode(props, JWT_SECRET, algorithm="HS256")
        headers = {"secret": token, "timespan": str(NEW_CONFIG_FREQ)}
        try:
            response = get(OUTPUT_PROCESSING_URL, headers=headers)
            with open("/data/history.yaml", "a", encoding="utf_8") as out_file:
                vals = response.json()
                overlay_stats = {}
                for overlay in overlay_names:
                    overlay_stats[overlay] = vals[overlay]
                formatted_output = {vals["timestamp"]: overlay_stats}
                logger.info(formatted_output)
                yaml.safe_dump(formatted_output, out_file)

        except RequestException:
            logger.exception(
                "Error when trying to get stats, probably output_processing is having trouble with postgres again",
                exc_info=1,
            )
        logger.info("sleeping %i seconds", ARCHIVE_ITER_SEC)
        sleep(ARCHIVE_ITER_SEC)


if __name__ == "__main__":
    main()
