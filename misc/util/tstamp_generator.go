package main

import (
	"flag"
	"fmt"
	"log"
	"time"
)

var startDate = flag.String("from", "", "Starting date of the time window")
var endDate = flag.String("to", "", "Ending date of the time window")

func main() {
	flag.Parse()

	if *startDate == "" {
		log.Fatal("no start date given")
	}
	if *endDate == "" {
		log.Fatal("no end date given")
	}

	startDateTime, err := time.Parse("2006-01-02", *startDate)
	if err != nil {
		log.Fatal(err)
	}
	endDateTime, err := time.Parse("2006-01-02", *endDate)
	if err != nil {
		log.Fatal(err)
	}

	startTimeStamp := startDateTime.Unix()
	endTimeStamp := endDateTime.Unix()

	if startTimeStamp > endTimeStamp {
		log.Fatal("Start date is after end date\n")

	}
	fmt.Printf("TIMESPAN_START_STAMP=%d\n", startTimeStamp)
	fmt.Printf("TIMESPAN_END_STAMP=%d\n", endTimeStamp)

}
