source ./.env

rsync -azP \
--exclude artemis-web/node_modules/  \
--exclude */.pytest_cache \
--exclude */.vscode \
--exclude .pytest_cache \
--exclude .vscode \
--exclude artemis-web/.git/ \
{config_generation,output_processing} ${REMOTE_HOST}:~/artemis/distr_hijack/
rsync -azP artemis-web/apps/artemis-web/pages/distrstats.tsx ${REMOTE_HOST}:~/artemis/distr_hijack/artemis-web/apps/artemis-web/pages/distrstats.tsx
rsync -azP artemis-web/apps/artemis-web/components/desktop-header/desktop-header.tsx ${REMOTE_HOST}:~/artemis/distr_hijack/artemis-web/apps/artemis-web/components/desktop-header/desktop-header.tsx
rsync -azP artemis-web/apps/artemis-web/components/mobile-header/mobile-header.tsx ${REMOTE_HOST}:~/artemis/distr_hijack/artemis-web/apps/artemis-web/components/mobile-header/mobile-header.tsx
rsync -azP artemis-web/Dockerfile ${REMOTE_HOST}:~/artemis/distr_hijack/artemis-web/Dockerfile
rsync -azP docker-compose.yaml ee-tik-nsgvm008.ethz.ch:~/artemis/docker-compose.yaml
rsync -zP nginx.conf ${REMOTE_HOST}:~/artemis/local_configs/frontend/nginx.conf
ssh ${REMOTE_HOST} ./refresh_custom.sh
echo $(date)
