"""Module to check whether any hijacks detected by artemis actually include
tracked ip addresses for one of the tracked overlays. Will return stats per
overlay network"""
import logging
from datetime import datetime, timedelta
from ipaddress import ip_address, ip_network
from os import environ
from os.path import exists, getmtime
from sys import stdout
from time import time

import matplotlib.pyplot as plt
from psycopg2.errors import OperationalError
from redis import Redis
from sqlalchemy import Column, create_engine
from sqlalchemy.dialects.postgresql import (
    ARRAY,
    BIGINT,
    BOOLEAN,
    INET,
    INTEGER,
    TEXT,
    TIMESTAMP,
    VARCHAR,
    JSON,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session


SECONDS_IN_A_DAY = 86400

# div by 60 because it is in seconds :facepalm:
STARTING_TIMESTAMP = int(environ.get("TIMESPAN_START_STAMP"))
ENDING_TIMESTAMP_raw = environ.get("TIMESPAN_END_STAMP")

if ENDING_TIMESTAMP_raw == "" or ENDING_TIMESTAMP_raw is None:
    ENDING_TIMESTAMP = int(datetime.now().timestamp())
else:
    ENDING_TIMESTAMP = int(ENDING_TIMESTAMP_raw)

REDIS_HOSTNAME = environ.get("REDIS_HOSTNAME")
DB_USER = environ.get("DB_USER")
DB_PASSWORD = environ.get("DB_PASS")
TESTING = bool(environ.get("TESTING", default=False))

if TESTING:
    DB_HOST = "thesis_postgres_1"
else:
    DB_HOST = "postgres"


redis_conn = Redis(host=REDIS_HOSTNAME)

Base = declarative_base()

logging.basicConfig(stream=stdout, level=logging.INFO)
logger = logging.getLogger()


class BgpUpdates(Base):
    """Wrapper class for the bgp_updates table
    in the artemis postgres db"""

    __tablename__ = "bgp_updates"
    key = Column("key", VARCHAR(32), nullable=False, primary_key=True)
    prefix = Column(INET)
    origin_as = Column(BIGINT)
    peer_asn = Column(BIGINT)
    as_path = Column(ARRAY(BIGINT))
    service = Column(VARCHAR(50))
    type = Column(VARCHAR(1))
    communities = Column(JSON)
    timestamp = Column(TIMESTAMP)
    hijack_key = Column(ARRAY(TEXT))
    handled = Column(BOOLEAN)
    matched_prefix = Column(INET)
    orig_path = Column(JSON)

    def __repr__(self) -> str:
        return f"{self.key} # {self.origin_as} - {self.prefix}"


class Hijacks(Base):
    """Wrapper class for the hijacks table
    in the artemis postgres db"""

    __tablename__ = "hijacks"
    key = Column("key", VARCHAR(32), nullable=False, primary_key=True)
    type = Column("type", VARCHAR(7))
    prefix = Column("prefix", INET)
    hijack_as = Column("hijack_as", BIGINT)
    peers_seen = Column("peers_seen", ARRAY(BIGINT))
    peers_withdrawn = Column(ARRAY(BIGINT))
    num_peers_seen = Column(INTEGER)
    asns_inf = Column(ARRAY(BIGINT))
    num_asns_inf = Column(INTEGER)
    time_started = Column(TIMESTAMP)
    time_last = Column(TIMESTAMP)
    time_ended = Column(TIMESTAMP)
    mitigation_started = Column(TIMESTAMP)
    time_detected = Column(TIMESTAMP)
    under_mitigation = Column(BOOLEAN)
    resolved = Column(BOOLEAN)
    active = Column(BOOLEAN)
    ignored = Column(BOOLEAN)
    withdrawn = Column(BOOLEAN)
    outdated = Column(BOOLEAN)
    dormant = Column(BOOLEAN)
    configured_prefix = Column(INET)
    timestamp_of_config = Column(TIMESTAMP)
    comment = Column(TEXT)
    seen = Column(BOOLEAN)
    community_annotation = Column(TEXT)
    rpki_status = Column(VARCHAR(2))

    def __repr__(self) -> str:
        return f"{self.key} - {self.hijack_as} -> {self.prefix} ## {self.configured_prefix}"


def get_stats(specific_overlay: str = None) -> dict:
    """Returns a dict with hijacked nodes per overlay network and a timestamp.
    Also number of total recently online nodes per overlay"""
    try:
        artemis_db_engine = create_engine(
            f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/artemis_db"
        )
    except OperationalError:
        logger.info("operational error, will exit for now")
        # assume is because db starting up
        return None

    overlay_names = list(map(lambda x: x.decode(), redis_conn.smembers("overlays")))
    logger.debug(overlay_names)

    with Session(bind=artemis_db_engine) as session:
        res: list[Hijacks] = session.query(Hijacks).all()

        result = {"timestamp": -1}

        for overlay in overlay_names:
            num_nodes_key = f"{overlay}_numnodes"
            overlay_num_nodes = (
                -1
                if (redis_val := redis_conn.get(num_nodes_key)) is None
                else int(redis_val)
            )
            result[overlay] = {"total_nodes": overlay_num_nodes, "hijacked_ips": dict()}
        monitor_ending_dt = datetime.fromtimestamp(ENDING_TIMESTAMP)
        monitor_starting_dt = datetime.fromtimestamp(STARTING_TIMESTAMP)
        for hijack in res:
            if (
                hijack.time_last < monitor_starting_dt
                or hijack.time_started > monitor_ending_dt
            ):
                # hijack isnt inside monitored timeframe
                # logger.debug(
                #     "skipping hijack from %i to %i because its outside of the timespan (%i - %i)",
                #     hijack.time_started.timestamp(),
                #     hijack.time_last.timestamp(),
                #     STARTING_TIMESTAMP,
                #     ENDING_TIMESTAMP,
                # )
                continue
            else:
                logger.debug("keeping hijack %s", hijack.key)
            config_prefix = hijack.configured_prefix
            hijacked_prefix = ip_network(str(hijack.prefix))
            for overlay in overlay_names:
                # if called for the stripped down stats,
                # skip unwanted overlays
                if specific_overlay and overlay != specific_overlay:
                    continue

                overlay_prefix_key = f"{overlay}_{str(config_prefix)}"
                possible_node_ips_int = redis_conn.smembers(overlay_prefix_key)
                for node_ip in map(lambda x: ip_address(int(x)), possible_node_ips_int):
                    if node_ip in hijacked_prefix:
                        ip_timestamps_key = f"{overlay}_{int(node_ip)}"
                        possible_timestamps = redis_conn.smembers(ip_timestamps_key)
                        # check whether the node was actually online during the reported hijack++
                        for tstamp in map(int, possible_timestamps):
                            logger.debug("ip: %s @ %i", str(node_ip), tstamp)
                            if hijack.time_started <= datetime.fromtimestamp(
                                tstamp
                            ) <= hijack.time_last or (
                                hijack.time_started <= datetime.fromtimestamp(tstamp)
                                and hijack.active
                            ):
                                logger.debug("keeping node, hijack: %s", hijack.key)
                                node_ip_str = str(node_ip)
                                if node_ip_str in result[overlay]["hijacked_ips"]:
                                    result[overlay]["hijacked_ips"][node_ip_str].add(
                                        tstamp
                                    )
                                else:
                                    result[overlay]["hijacked_ips"][node_ip_str] = set(
                                        [tstamp]
                                    )
                            else:
                                logger.debug("tstamp doesnt match hijack")
                                logger.debug(
                                    "hijacked stared: %s", str(hijack.time_started)
                                )
                                logger.debug("hijack ended: %s", str(hijack.time_ended))
                                logger.debug(
                                    "node online: %s",
                                    str(datetime.fromtimestamp(tstamp)),
                                )

        session.commit()
        session.close()

    # make it valid json -> set to list
    for overlay_name in overlay_names:
        for hijacked_ip in result[overlay_name]["hijacked_ips"]:
            result[overlay_name]["hijacked_ips"][hijacked_ip] = list(
                result[overlay_name]["hijacked_ips"][hijacked_ip]
            )
    result["timestamp"] = int(time())
    return result


def make_axes(stats: dict[str, dict], granularity: int) -> tuple[list, list]:
    """Generate y values and x labels given the
    result of the hijack analysis"""

    def date_to_string(datetime_object: datetime) -> str:
        res = str(datetime_object)
        time_diff = timedelta(seconds=granularity)
        res2 = str(datetime_object + time_diff)
        if granularity >= SECONDS_IN_A_DAY:
            out = f"{res.split(' ',maxsplit=1)[0]}\n- {res2.split(' ',maxsplit=1)[0]}"  # YYYY-MM-DD
        else:
            res = res[:16]  # YYYY-MM-DD hh:mm
            res2 = res2[:16]
            out = f"{res}\n- {res2}"
        return out

    num_elems = (ENDING_TIMESTAMP - STARTING_TIMESTAMP) // granularity + 1
    y_axis_sets: list[set] = [None] * (num_elems)
    for i in range(0, num_elems):
        y_axis_sets[i] = set()
    y_axis = [0] * (num_elems + 1)
    x_axis = [
        date_to_string(datetime.fromtimestamp(STARTING_TIMESTAMP + granularity * x))
        for x in range(0, num_elems + 1)
    ]

    get_index = lambda timestamp: (timestamp - STARTING_TIMESTAMP) // granularity

    # should only be one overlay anyway
    for overlay_name, stat_map in stats.items():
        if overlay_name == "timestamp":
            continue
        for ip_addr, tstamps in stat_map["hijacked_ips"].items():
            for tstamp in tstamps:
                y_axis_sets[get_index(tstamp)].add(ip_addr)
    for i in range(0, num_elems):
        y_axis[i] = len(y_axis_sets[i])
    return x_axis, y_axis


def get_image(overlay_name: str) -> str:
    """creates a bar graph representing the number of hijacks per
     (dynamically chosen) time unit and returns a string containing
    the path to the image"""

    out_path = f"/tmp/{overlay_name}_{STARTING_TIMESTAMP}-{ENDING_TIMESTAMP}.png"
    # if graph already exists and is not older than 1 minutes
    if exists(out_path) and datetime.now().timestamp() - getmtime(out_path) <= 60:
        # no need to regenerate the exact same image
        logger.info("Using already existing graph")
        return out_path
    overlay_names = list(map(lambda x: x.decode(), redis_conn.smembers("overlays")))
    if overlay_name not in overlay_names:
        logger.warning(
            "invalid overlay name %s not in overlays %s",
            overlay_name,
            ", ".join(overlay_names),
        )
        return None

    fig = plt.figure(layout="constrained")
    axes: plt.Axes = fig.add_subplot(111)
    if ENDING_TIMESTAMP - STARTING_TIMESTAMP < SECONDS_IN_A_DAY:
        granularity = 3600
    else:
        granularity = SECONDS_IN_A_DAY
    domain, codomain = make_axes(get_stats(overlay_name), granularity)
    axes.bar(domain, codomain)
    plt.xticks(rotation=45)
    plt.xlabel("Time")
    plt.ylabel("# Nodes affected by hijack")
    plt.savefig(out_path)
    return out_path


if __name__ == "__main__":
    get_stats()
