"""API for the analysis of the hijacks"""
import logging
from os import environ
from sys import stdout

import jwt
from flask import Flask, abort, request, send_file

import process_hijacks

logging.basicConfig(stream=stdout, level=logging.INFO)
logger = logging.getLogger()
SECRET = environ.get("JWT_SECRET")

app = Flask(__name__)


@app.route("/get_stats")
def return_stats():
    """handling the api endpoint to get JSON stats"""
    logger.info("/get_stats")
    req_token = request.headers.get("secret")
    try:
        if check_auth(req_token):
            processing_result = process_hijacks.get_stats()
            if process_hijacks is None:
                abort(400)  # if invalid overlay name is given
            return processing_result
        else:
            abort(401)
    except ValueError:
        abort(400)


@app.route("/get_graph")
def return_graph():

    """handling the api endpoint to get
    graphical representation of the stats"""
    logger.info("/get_graph")
    req_token = request.args.get("secret")
    overlay = request.args.get("overlay").lower()
    try:
        if check_auth(req_token):

            img_path = process_hijacks.get_image(overlay)
            if img_path is None:
                logger.warning("no image path returned")
                abort(400)
            return send_file(img_path)
        abort(401)
    except ValueError:
        logger.fatal("value error", exc_info=1)
        abort(400)


def check_auth(jwt_token: str) -> bool:
    """checks wether a provided jwt token
    is actually validTODO add check for age of token"""
    try:
        decoded_jwt = jwt.decode(jwt=jwt_token, key=SECRET, algorithms="HS256")
        logger.info("auth'd %s", decoded_jwt["user"]["email"])
        return True
    except jwt.exceptions.DecodeError:
        return False
