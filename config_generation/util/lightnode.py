"""Util module for the generic LightNode"""
from typing import Union
from ipaddress import IPv4Address, IPv6Address
from util.ip import IpVersion


class LightNode:
    """
    Minimal representation of a node in an overlay network;
    all overlay network modules should export to this class
    """

    def __init__(
        self,
        ip_addr: Union[IPv4Address, IPv6Address],
        asn: int,
        ipversion: IpVersion,
        snapshot_timestamp: int,
    ):
        self.asn = asn
        self.ip_addr = ip_addr
        self.ipversion = ipversion
        self.timestamp_seen = snapshot_timestamp

    def __repr__(self) -> str:
        return f"{self.ip_addr} @ {self.timestamp_seen}"

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, LightNode):
            return False
        return (
            __o.ip_addr == self.ip_addr
            and __o.asn == self.asn
            and __o.timestamp_seen == self.timestamp_seen
        )

    def __hash__(self) -> int:
        return hash(self.ip_addr)
