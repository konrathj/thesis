"""Module providing classes to represent ip addresses and
prefixes as well as helper methods around that"""

from enum import Enum, auto
from ipaddress import IPv4Address, IPv4Network, IPv6Address, IPv6Network
from typing import Union

DEBUG = False


class IpVersion(Enum):
    """Enum for IP Versions"""

    IPV6 = auto()
    IPV4 = auto()


def get_ip_version(
    ip_addr_or_network: Union[IPv4Address, IPv4Network, IPv6Address, IPv6Network]
) -> IpVersion:
    """Returns the ip version of either an ip address or prefix"""
    if isinstance(ip_addr_or_network, (IPv4Address, IPv4Network)):
        return IpVersion.IPV4
    if isinstance(ip_addr_or_network, (IPv6Address, IPv6Network)):
        return IpVersion.IPV6
    print(ip_addr_or_network)
    raise ValueError("wtf")
