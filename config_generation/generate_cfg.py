"""used to generate a config file for artemis that covers all
currently announced prefixes that contain the ip addresses of tracked
nodes and then push that config to an artemis instance"""
import csv
import datetime
import json
import logging
from ipaddress import ip_address, ip_network
from os import environ, path
from sys import stdout
from time import sleep
from typing import Callable

import pyasn
import pybgpstream
import requests
import ujson
import yaml
from redis import Redis
from requests.exceptions import ReadTimeout

from node_ip_collection.bitcoin import BitcoinIpCollector

from ripe_data_collection.fetch_routing_table import (
    AnnouncedPrefix,
    RipeAnnouncementsParser,
)
from util.ip import IpVersion
from util.lightnode import LightNode

logging.basicConfig(stream=stdout, level=logging.INFO)
logger = logging.getLogger()

REDIS_HOSTNAME = environ.get("REDIS_HOSTNAME")
SEND_TO_ARTEMIS = True
if int(environ.get("TESTING", default=0)) > 0:
    logger.warning("Will not try to push config to artemis")
    SEND_TO_ARTEMIS = False
ARTEMIS_CONFIG_PATH = path.join(path.dirname(__file__), "config.yaml")
LATEST_ROUTING_TABLE_PATH = "/data/latest-table.bz"

STARTING_TIMESTAMP = int(environ.get("TIMESPAN_START_STAMP"))
ENDING_TIMESTAMP = int(
    environ.get("TIMESPAN_END_STAMP", default=datetime.datetime.now().timestamp())
)

NOTIFY = True
DISCORD_WEBHOOK = environ.get("DISCORD_WEBHOOK_LINK", default="")
if DISCORD_WEBHOOK == "":
    NOTIFY = False

IGNORE_6TO4 = True  # default case
ignore_6to4_env = environ.get("IGNORE_6TO4")
if ignore_6to4_env and ignore_6to4_env.lower() == "false":
    IGNORE_6TO4 = False

if ignore_6to4_env and ignore_6to4_env.lower() not in ("false", "true"):
    logger.warning(
        "%s is not a valid value for IGNORE_6TO4, must be either 'true' or 'false'",
        ignore_6to4_env,
    )


OVERLAY_NETWORKS: list[tuple[str, Callable[[int, int], set[LightNode]]]] = (
    (
        "bitcoin",
        BitcoinIpCollector().setup_and_return_light_nodes,
    ),
    (
        "youtube-test",
        lambda from_timestamp, to_timestamp: [
            LightNode(
                ip_addr=ip_address("208.65.153.238"),
                asn=36561,
                ipversion=IpVersion.IPV4,
                snapshot_timestamp=1203889980,
            )
        ],
    ),
    # ("tor", set),
    # ("some_overlay", some_method),
)

redis_conn = Redis(host=REDIS_HOSTNAME)
overlay_names = list(map(lambda x: x[0], OVERLAY_NETWORKS))


def generate_yaml_dict(prefixes_by_asn: dict[int, set[AnnouncedPrefix]]) -> dict:
    """given a mapping of asn->prefixes this will generate an artemis config in dict form"""
    output_prefixes = {}
    output_monitors = {
        "riperis": [""],
        "bgpstreamlive": ["routeviews", "ris"],
        "bgpstreamhist": "/csv_dir",
    }
    output_asns = {}
    output_rules = []
    for asn, asn_prefixes in prefixes_by_asn.items():
        prefix_section_name = f"{asn}_prefixes"
        output_prefixes[prefix_section_name] = []
        for prefix_object in asn_prefixes:
            output_prefixes[prefix_section_name].append(str(prefix_object.prefix))
        asn_section_name = f"{asn}_asn"
        output_asns[asn_section_name] = [asn]
        output_rules_dict = {
            "prefixes": output_prefixes[prefix_section_name],
            "origin_asns": output_asns[asn_section_name],
            "neighbors": [],
            "mitigation": "manual",
        }
        output_rules.append(output_rules_dict)

    output = {
        "prefixes": output_prefixes,
        "monitors": output_monitors,
        "asns": output_asns,
        "rules": output_rules,
    }
    return output


def community_list(value):
    """Copied from artemis' conversion script"""
    com_list = []
    for i in value:
        asn_val_pair = i.split(":")
        asn_val_dict = {"asn": int(asn_val_pair[0]), "value": int(asn_val_pair[1])}
        com_list.append(asn_val_dict)
    json_dump = ujson.dumps(com_list)
    return json_dump


def load_updates_bgpstream(prefixes_by_asn: dict[int, set[AnnouncedPrefix]]):
    """given the mapping from asn to announced
    prefixes that contain active nodes, use bgpstream to
    download all bgp updates affecting those prefixes during the
    monitored timespan"""
    filter_string = "prefix"
    for _asn, asn_prefixes in prefixes_by_asn.items():
        for prefix in asn_prefixes:
            filter_string += f" any {prefix.prefix}"

    stream = pybgpstream.BGPStream(
        from_time=STARTING_TIMESTAMP,
        until_time=ENDING_TIMESTAMP,
        collectors=["rrc00"],
        record_type="updates",
        filter=filter_string,
    )
    out_file = f"/csv_dir/updates_{datetime.datetime.now().timestamp()//1}.csv"
    with open(out_file, "w", encoding="utf-8") as out_file_object:
        csv_writer = csv.writer(out_file_object, delimiter="|")
        elemcount = 0
        for elem in stream:
            elemcount += 1
            if elemcount % 10000 == 0:
                logger.info("Update #%i", elemcount)
            if (elem.status != "valid") or (elem.record.rec.type != "update"):
                continue
            if elem.type in ["A", "W"]:
                elem_csv_list = []
                if elem.type == "A":
                    elem_csv_list = [
                        str(elem.fields["prefix"]),
                        str(elem.fields["as-path"].split(" ")[-1]),
                        str(elem.peer_asn),
                        str(elem.fields["as-path"]),
                        str(elem.project),
                        str(elem.collector),
                        str(elem.type),
                        community_list(elem.fields["communities"]),
                        str(elem.time),
                    ]
                else:
                    elem_csv_list = [
                        str(elem.fields["prefix"]),
                        "",
                        str(elem.peer_asn),
                        "",
                        str(elem.project),
                        str(elem.collector),
                        str(elem.type),
                        ujson.dumps([]),
                        str(elem.time),
                    ]
                csv_writer.writerow(elem_csv_list)


def gen_config(ipasn_file: str, oldest_timestamp: int, newest_timestamp: int) -> None:
    """
    takes ipasn.dat filename/path as input and (using the bitnode api)
    generates a config for ARTEMIS BGP that covers all current nodes on the overlay networks
    and writes the config to ARTEMIS_CONFIG_PATH  (as yaml)
    """
    logger.info("reading ip db")
    asndb = pyasn.pyasn(ipasn_file)
    logger.info("getting node ip addresses")
    overlay_nodes: dict[str, set[LightNode]] = {}
    for overlay_name, collector_method in OVERLAY_NETWORKS:
        logger.info("getting ips for %s", overlay_name)
        overlay_nodes[overlay_name] = collector_method(
            from_timestamp=oldest_timestamp, to_timestamp=newest_timestamp
        )

    logger.info("processing collected data")
    # with transaction set to True the enqueued commands will be executed atomically
    pipeline = redis_conn.pipeline(transaction=True)
    pipeline.flushall()
    pipeline.sadd("overlays", *overlay_names)

    prefixes_by_asn: dict[int, set[AnnouncedPrefix]] = {}
    for overlay_name, nodes in overlay_nodes.items():
        found_ip_addresses = set()
        numnodes_key = f"{overlay_name}_numnodes"
        for node in nodes:
            if IGNORE_6TO4:
                # per ip_address docs, if an ipv6 address is not a 6to4 address
                # the "sixtofour" field will simply be None
                # hence this checks for an address being ipv6 && being 6to4
                if (
                    node.ipversion == IpVersion.IPV6
                    and node.ip_addr.sixtofour is not None
                ):
                    continue
            asn, matching_prefix = asndb.lookup(format(node.ip_addr))
            if matching_prefix is None:
                logger.warning("%s no matching prefix found", node)
                continue
            prefix_object = AnnouncedPrefix(ip_network(matching_prefix), asn)
            prefix_set_key = f"{overlay_name}_{str(prefix_object.prefix)}"
            pipeline.sadd(prefix_set_key, int(node.ip_addr))
            # mapping from ip addresses to when the nodes were active
            ip_timestamps_key = f"{overlay_name}_{int(node.ip_addr)}"
            pipeline.sadd(ip_timestamps_key, node.timestamp_seen)
            found_ip_addresses.add(int(node.ip_addr))
            if asn in prefixes_by_asn:
                prefixes_by_asn[asn].add(prefix_object)
            else:
                prefixes_by_asn[asn] = {
                    prefix_object,
                }
        num_nodes = len(found_ip_addresses)
        pipeline.set(numnodes_key, num_nodes)
    pipeline.execute()  # run the commands

    logger.info("Getting bgpstream updates")
    load_updates_bgpstream(prefixes_by_asn)
    logger.info("generating config structure")

    # PRINT COMMAND FOR GETTING ENTIRE HISTORY IN TIMEFRAME FOR ALL PREFIXES

    final_config = generate_yaml_dict(prefixes_by_asn)
    logger.info("exporting config")
    with open(ARTEMIS_CONFIG_PATH, "w", encoding="utf_8") as file:
        yaml.safe_dump(final_config, file)


def update_artemis(ipasn_file: str, oldest_timestamp: int, newest_timestamp: int):
    """takes the path to a pyasn ipasn database file which will be
    used in combination with for example bitnode API to generate an
    artemis config that covers all currently online ipv4/ipv6 bitcoin
    nodes. This config will then be pushed to an artemis instance"""
    logger.info("generating config")
    gen_config(
        ipasn_file, oldest_timestamp=oldest_timestamp, newest_timestamp=newest_timestamp
    )
    with open(ARTEMIS_CONFIG_PATH, "r", encoding="utf_8") as file:
        yaml_lines = file.readlines()
    data = json.dumps({"type": "yaml", "content": yaml_lines})
    logger.info("sending config to artemis [timeout is 120s]")
    if SEND_TO_ARTEMIS:
        try:
            response = requests.post(
                "http://artemis_configuration_1:3000/config", data=data, timeout=120
            )
            logger.info(response.status_code, response.json())
            return True
        except (TimeoutError, ReadTimeout):
            logger.warning(
                "Got timeout when trying to send to artemis... will retry in 3 minutes"
            )
            return False
    else:
        logger.info("Not actually sending to artemis...")
        logger.info(
            '#requests.post("http://artemis_configuration_1:3000/config", data=data, timeout=)'
        )
        return True


def send_notification(message: str) -> None:
    data = {
        "username": "Thesis Notifier",
        "embeds": [
            {"title": "Preprocessing info", "description": message, "color": "12344"}
        ],
    }

    requests.post(DISCORD_WEBHOOK, json=data)


def file_older_than(fpath: str, treshold: datetime.timedelta) -> bool:
    """returns if the file at `fpath` has last been modified more than `treshold` ago"""
    mod_tstamp = datetime.datetime.fromtimestamp(path.getmtime(fpath))
    now_tstamp = datetime.datetime.now()
    return now_tstamp - mod_tstamp >= treshold


if __name__ == "__main__":
    processing_start_time = datetime.datetime.now().timestamp()
    if NOTIFY:
        logger.info("sending starting notification")
        send_notification("Starting")

    starting_pipeline = redis_conn.pipeline()
    logger.info("Downloading routing table...")
    parser = RipeAnnouncementsParser()
    # below line uses default value for monitor=RRC0
    ipasn_dat_file = parser.get_routing_table(
        LATEST_ROUTING_TABLE_PATH, tstamp=STARTING_TIMESTAMP
    )

    while True:  # repeat until successfully pushing new configuration file
        if not path.exists(LATEST_ROUTING_TABLE_PATH) or file_older_than(
            LATEST_ROUTING_TABLE_PATH, datetime.timedelta(hours=8)
        ):
            logger.info("Downloading routing table...")
            parser = RipeAnnouncementsParser()
            ipasn_dat_file = parser.get_routing_table(LATEST_ROUTING_TABLE_PATH)
        else:
            logger.info("Reusing routing table")
            ipasn_dat_file = path.join(
                path.dirname(LATEST_ROUTING_TABLE_PATH), "ipasn.dat"
            )
        SUCCESS = update_artemis(
            ipasn_dat_file,
            oldest_timestamp=STARTING_TIMESTAMP,
            newest_timestamp=ENDING_TIMESTAMP,
        )
        if SUCCESS:
            logger.info(
                "Successfully pushed configuration covering %i to %i",
                STARTING_TIMESTAMP,
                ENDING_TIMESTAMP,
            )
            break  # stop retrying

        logger.info("Didn't succeed, will retry")
        sleep(180)

    processing_end_time = datetime.datetime.now().timestamp()
    duration = int(processing_end_time - processing_start_time)

    logger.info("Finished after %is", duration)
    if NOTIFY:
        logger.info("sending ending notification")
        send_notification(f"DONE after {duration}s!!")
