"""Module to collect ip addresses of nodes in the bitcoin network"""
import datetime
from ipaddress import IPv4Address, IPv6Address, ip_address
from sys import stdout
from typing import Union
import logging

import requests
from util.ip import IpVersion
from util.lightnode import LightNode

DEBUG = False
API_URL = "https://bitnodes.io/api/v1/"
SNAPSHOTS_URL = API_URL + "snapshots/"


logging.basicConfig(stream=stdout, level=logging.INFO)
logger = logging.getLogger()


def clean_port(ip_str: str) -> str:
    """Strip port from a string representing
    either an ipv4 or ipv6 address"""
    if (
        ip_str[0] == "["
    ):  # ipv6 have format [a:b:c:d:e:f:g:h]:1234 where 1234 is the port
        return ip_str.split("]")[0].lstrip("[")
    return ip_str.split(":")[0]


class BitcoinNode:
    """Class to wrap api object Node"""

    def __init__(self, ip_str: str, info: list, snapshot_timestamp):
        self.snapshot_tstamp: int = snapshot_timestamp
        self.ip_addr = ip_address(clean_port(ip_str))
        (
            self.protocol,
            self.user_agent,
            self.connected_since,
            self.services,
            self.height,
            self.hostname,
            self.city,
            self.country_code,
            self.latitude,
            self.longitude,
            self.timezone,
            self.asn,
            self.org_name,
        ) = info
        if isinstance(self.ip_addr, IPv4Address):
            self.ipversion = IpVersion.IPV4
        elif isinstance(self.ip_addr, IPv6Address):
            self.ipversion = IpVersion.IPV6
        else:
            logger.warning(self.ip_addr)
            raise ValueError("Unknown ip protocol??")

    def make_light(self):
        """Convert a `BitcoinNode` Object to a generic `LightNode`"""
        return LightNode(self.ip_addr, self.asn, self.ipversion, self.snapshot_tstamp)

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, BitcoinNode):
            return False
        if (
            __o.ip_addr == self.ip_addr
            and __o.country_code == self.country_code
            and self.asn == __o.asn
        ):
            return True
        return False

    def __hash__(self) -> int:
        return hash(self.ip_addr)

    def __repr__(self) -> str:
        return str(self.ip_addr) + " / " + self.hostname


class BitcoinSnapshot:
    """Class to wrap the api and store info per Snapshot"""

    def __init__(self, snapshot_info: dict):
        self.url = snapshot_info["url"]
        self.timestamp = int(snapshot_info["timestamp"])
        self.total_nodes = snapshot_info["total_nodes"]
        self.latest_height = snapshot_info["latest_height"]

    def __hash__(self) -> int:
        """Redefined to be able to use with sets"""
        return self.timestamp

    def __eq__(self, __o: object) -> bool:
        """Redefined to be able to use with sets"""
        if isinstance(__o, BitcoinSnapshot):
            if __o.timestamp == self.timestamp and self.url == __o.url:
                return True

        return False

    def get_ip_addresses(self) -> list[BitcoinNode]:
        """return a list of ip addresses collected in snapshot"""
        res = requests.get(self.url).json()
        ip_count = 0
        onion = 0
        nodes = []
        for k in res["nodes"].keys():
            if "onion" in k:  # tor network address
                onion += 1
            elif k[0].isnumeric() or k[0] == "[":  # ipv4/v6 addresses
                ip_count += 1
                nodes.append(BitcoinNode(k, res["nodes"][k], self.timestamp))
            else:
                raise ValueError("Weird IP address found: " + k)
        if DEBUG:
            print(f"{ip_count=},{onion=}")
        return nodes


class BitcoinIpCollector:
    """Ip Collection Class for the bitcoin Network specifically"""

    def __init__(self):
        self.snapshots: set[BitcoinSnapshot] = set()
        self.collected_snapshots: set[BitcoinSnapshot] = set()
        self.nodes: set[Union[BitcoinNode, LightNode]] = set()

    def collect_snapshots(self, newest_timestamp: int, oldest_timestamp: int = None):
        """Update the list of snapshots with the latest available ones"""
        res = requests.get(SNAPSHOTS_URL + "?limit=100").json()
        if oldest_timestamp is None:
            for snap_info in res["results"]:
                self.snapshots.add(BitcoinSnapshot(snap_info))
        else:
            young_enough = True
            while young_enough:
                next_page_link = res["next"]
                logger.info("[bitcoin] new page %s", next_page_link)
                for snap_info in res["results"]:

                    # check that snapshot is within time window
                    if snap_info["timestamp"] < oldest_timestamp:
                        # end of window reached, can exit for loop
                        young_enough = False
                        break

                    if snap_info["timestamp"] > newest_timestamp:
                        logger.debug(
                            "[bitcoin] snapshot too young: %i > %i",
                            snap_info["timestamp"],
                            newest_timestamp,
                        )
                        continue  # snapshot too young

                    # else add snapshot
                    self.snapshots.add(BitcoinSnapshot(snap_info))
                if next_page_link is None:  # last page reached
                    young_enough = False
                    continue
                res = requests.get(next_page_link).json()

    def collect_ip_addresses(
        self,
        newest_timestamp: int,
        update_snapshots: bool = True,
        make_light: bool = False,
        oldest_timestamp: int = None,
    ):
        """collects all ip addressses in the collected snapshots"""
        if update_snapshots:
            self.collect_snapshots(
                newest_timestamp=newest_timestamp, oldest_timestamp=oldest_timestamp
            )
        num_snaps = len(self.snapshots)
        for indx, snap in enumerate(self.snapshots):
            if indx % 10 == 0:
                logger.info("Processing snapshot %i/%i", indx + 1, num_snaps)

            if snap in self.collected_snapshots:
                continue
            nodes = snap.get_ip_addresses()
            for node in nodes:
                if make_light:
                    self.nodes.add(node.make_light())
                else:
                    self.nodes.add(node)
            self.collected_snapshots.add(snap)

    def setup_and_return_light_nodes(
        self, from_timestamp: int, to_timestamp: int = None
    ) -> set[LightNode]:
        """One-does-all method that sets up the collector and
        returns the list of ip addresses of nodes contained in
        the latest available snapshots"""
        self.snapshots.clear()  # so no "old" data is still present
        self.collected_snapshots.clear()
        self.nodes.clear()
        if to_timestamp is None:  # window end is optional
            logger.info("no to_timestamp, using current time")
            to_timestamp = int(datetime.datetime.now().timestamp())
        self.collect_ip_addresses(
            newest_timestamp=to_timestamp,
            update_snapshots=True,
            make_light=True,
            oldest_timestamp=from_timestamp,
        )
        return self.nodes


# TODO yeet the whole collector class thing... utterly useless
# leave for now... 15.6.2022

if __name__ == "__main__":
    print("Not supposed to be run on its own")
