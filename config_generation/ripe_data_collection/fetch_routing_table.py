# pylint: disable=no-member
"""Module that handles downloading a routing table from ripe ris"""
from datetime import datetime as dt
from datetime import timedelta as td
from enum import Enum
from ipaddress import IPv4Network, IPv6Network, ip_network
from os import path, system
from typing import Union

import requests
import re
from bgpdumpy import BGPDump, TableDumpV2
from bgpdumpy.BGPDump import TableDumpV2RouteEntry
from util.ip import IpVersion, get_ip_version

DEBUG = False
LIMIT_COUNT = False
PARSE_LIMIT = 20
test_file_path = path.join(path.dirname(__file__), "testing-updates.mrt")


class BGPMonitors(Enum):
    "Enum for the public BGP monitors provided by RIPE"
    # commented means no longer active
    RRC00 = "https://data.ris.ripe.net/rrc00/"
    RRC01 = "https://data.ris.ripe.net/rrc01/"
    # RRC02 = "https://data.ris.ripe.net/rrc02/"
    RRC03 = "https://data.ris.ripe.net/rrc03/"
    RRC04 = "https://data.ris.ripe.net/rrc04/"
    RRC05 = "https://data.ris.ripe.net/rrc05/"
    RRC06 = "https://data.ris.ripe.net/rrc06/"
    RRC07 = "https://data.ris.ripe.net/rrc07/"
    # RRC08 = "https://data.ris.ripe.net/rrc08/"
    # RRC09 = "https://data.ris.ripe.net/rrc09/"
    RRC10 = "https://data.ris.ripe.net/rrc10/"
    RRC11 = "https://data.ris.ripe.net/rrc11/"
    RRC12 = "https://data.ris.ripe.net/rrc12/"
    RRC13 = "https://data.ris.ripe.net/rrc13/"
    RRC14 = "https://data.ris.ripe.net/rrc14/"
    RRC15 = "https://data.ris.ripe.net/rrc15/"
    RRC16 = "https://data.ris.ripe.net/rrc16/"
    RRC17 = "https://data.ris.ripe.net/rrc17/"
    RRC18 = "https://data.ris.ripe.net/rrc18/"
    RRC19 = "https://data.ris.ripe.net/rrc19/"
    RRC20 = "https://data.ris.ripe.net/rrc20/"
    RRC21 = "https://data.ris.ripe.net/rrc21/"
    RRC22 = "https://data.ris.ripe.net/rrc22/"
    RRC23 = "https://data.ris.ripe.net/rrc23/"
    RRC24 = "https://data.ris.ripe.net/rrc24/"
    RRC25 = "https://data.ris.ripe.net/rrc25/"
    RRC26 = "https://data.ris.ripe.net/rrc26/"


class AnnouncedPrefix:
    """More light-weight version of BGPAnnouncement to use for final extraction"""

    def __init__(self, prefix: Union[IPv4Network, IPv6Network], asn: int):
        self.asn = asn
        self.prefix = prefix

    def __repr__(self) -> str:
        return f"{self.prefix} | {self.asn}"

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, AnnouncedPrefix):
            return False
        return self.asn == __o.asn and self.prefix == __o.prefix

    def __hash__(self) -> int:
        return hash(self.prefix)


class RipeAnnouncementsParser:
    """Main class of this module used to extract all needed info  from a file with updates"""

    def __init__(self):
        self.read_filenames = set()

    @staticmethod
    def get_latest(out_fname, monitor=BGPMonitors.RRC00):
        """Downloads the latest list of bgp update for a given
        monitor node"""
        latest_remote_name = "latest-update.gz"
        url = monitor.value + latest_remote_name
        fname = path.join(path.dirname(__file__), out_fname)
        with open(fname, "wb") as file:
            result = requests.get(url)
            file.write(result.content)

    @staticmethod
    def generate_routing_table_link(monitor: BGPMonitors, tstamp: int) -> str:
        """will search for the youngest full routing table
        provided by the given monitor on the date one day before the timestamp
        that was given as an argument, this way it is guaranteed to have been
        dumped before the timestamp"""
        stamp_date = dt.fromtimestamp(tstamp)
        string_arg_date = stamp_date - td(days=1)
        string_arg = f"{string_arg_date.year}{str(string_arg_date.month).zfill(2)}{str(string_arg_date.day).zfill(2)}"
        index_link = f"{monitor.value}{string_arg_date.year}.{str(string_arg_date.month).zfill(2)}/"
        index_response = requests.get(index_link)
        re_pattern = fr'<a.*href="(.?view\.{string_arg}.*\.gz)"'
        filepaths = re.findall(re_pattern, index_response.content.decode())
        latest_full_link = index_link + filepaths[0]
        return latest_full_link

    @staticmethod
    def get_routing_table(out_fpath, monitor=BGPMonitors.RRC00, tstamp=None):
        """downloads the latest full routing table for a given
        monitor"""
        if tstamp is None:
            url = monitor.value + "latest-bview.gz"
        else:
            url = RipeAnnouncementsParser.generate_routing_table_link(monitor, tstamp)

        dirname = path.dirname(out_fpath)

        with requests.get(url, stream=True) as result:
            with open(out_fpath, "wb") as file:
                for chunk in result.iter_content(chunk_size=4096):
                    file.write(chunk)
        fname_ipasndb = f"{dirname}/ipasn.dat"
        system(f"pyasn_util_convert.py --single {out_fpath} {fname_ipasndb}")
        return fname_ipasndb

    @staticmethod
    def read_file(fname: str) -> list[AnnouncedPrefix]:
        """given path to a mrt file with bgp updates parse those and extract information in
        the form of BGPAnnouncement objects"""
        fname = fname.strip()
        fpath = path.join(path.dirname(__file__), fname)
        announcements_v4 = []
        annoumcements_v6 = []
        counter = 0
        with BGPDump(fpath) as bgp:
            for entry in bgp:
                if counter % 1000 == 0:
                    print(f"{counter=}")
                counter += 1

                if not isinstance(entry.body, TableDumpV2):
                    print("naw")
                    continue

                if entry.body.prefixLength == 0:
                    continue

                prefix = ip_network(f"{entry.body.prefix}/{entry.body.prefixLength}")
                ipversion = get_ip_version(prefix)
                route: TableDumpV2RouteEntry
                for route in entry.body.routeEntries:
                    as_path = route.attr.asPath.split()
                    neg_as_path_len = -len(as_path)
                    origin_index = -1
                    while "{" in as_path[origin_index]:
                        origin_index -= 1
                        if origin_index < neg_as_path_len:
                            print(as_path)
                            raise ValueError("all private asns??")
                    try:
                        origin_asn = int(as_path[origin_index])
                    except ValueError as error:
                        print(route.attr.asPath.split())
                        raise error
                    if ipversion == IpVersion.IPV4:
                        announcements_v4.append(AnnouncedPrefix(prefix, origin_asn))
                    else:
                        annoumcements_v6.append(AnnouncedPrefix(prefix, origin_asn))
        return announcements_v4, annoumcements_v6


if __name__ == "__main__" and DEBUG:
    test_parser = RipeAnnouncementsParser()
    test_updates = test_parser.read_file(test_file_path)
# NOTE maybe add argparse if __name__ == __main__ for cli usability
